<?php

use Codeception\Util\HttpCode;

class DeleteEmployeeCest
{
    public function _before(ApiTester $I)
    {
        $I->sendPOST('/auth/login', ['email' => $I->email, 'password' => $I->password]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseJsonMatchesJsonPath('$.access_token');
        $I->token = $I->grabDataFromResponseByJsonPath('$.access_token')[0];
    }

    // tests
    public function fireEmployeeTest(ApiTester $I)
    {
        $I->wantTo('Delete a Employee from database');
        $I->amBearerAuthenticated($I->token);
        $I->sendDELETE('/employees/643.842.640-70');
        $I->seeResponseCodeIs(HttpCode::NO_CONTENT);
    }

    public function lookForEmployeeDataTest(ApiTester $I)
    {
        $I->wantTo('Check if employee data was really deleted');
        $I->amBearerAuthenticated($I->token);
        $I->sendGET('/employees');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->dontSeeResponseContainsJson([
            'name' => 'Teobaldo Montenegro Sobrinho',
            'email' => 'teo@montenegro.com',
            'document' => '643.842.640-70',
            'city' => 'Ávila do Sul',
            'state' => 'Amazonas',
            'start_date' => '2018-05-01',
            'manager_id' => '41b6c1f7-1d2a-4050-a28d-4861a65d948f'
        ]);
    }
}
